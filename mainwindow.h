#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "inspector2equipment.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();
    void error(QString);
    void warning(QString);
    void information(QString);
private:
    Ui::MainWindow *ui;
    int xCount, yCount, deltaXlimit, deltaYlimit;
    Inspector2Equipment equip;
};

#endif // MAINWINDOW_H
