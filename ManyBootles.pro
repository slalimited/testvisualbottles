#-------------------------------------------------
#
# Project created by QtCreator 2018-07-11T18:51:41
#
#-------------------------------------------------

QT       += core gui network printsupport script serialport xml sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ManyBootles
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp

HEADERS += \
        mainwindow.h

FORMS += \
        mainwindow.ui

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/'../Inspector2Equipment-Desktop Qt 5.11.1 MSVC2017 64bit/release/' -lInspector2Equipment
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/'../Inspector2Equipment-Desktop Qt 5.11.1 MSVC2017 64bit/debug/' -lInspector2Equipment

INCLUDEPATH += $$PWD/../Inspector2Equipment
DEPENDPATH += $$PWD/../Inspector2Equipment
INCLUDEPATH += $$PWD/../BarcodeScanner
DEPENDPATH += $$PWD/../BarcodeScanner
INCLUDEPATH += $$PWD/../CommonDevice
DEPENDPATH += $$PWD/../CommonDevice
INCLUDEPATH += $$PWD/../MiPrinter
DEPENDPATH += $$PWD/../MiPrinter
INCLUDEPATH += $$PWD/../DesktopPrinter
DEPENDPATH += $$PWD/../DesktopPrinter
INCLUDEPATH += $$PWD/../DesktopPrinter/limereport/include
DEPENDPATH += $$PWD/../DesktopPrinter/limereport/include
win32-msvc {
    MSVC_VER = $$(VisualStudioVersion)
    equals(MSVC_VER, 14.0) {
        MSVC_VER = "MSVC2015_64bit"
    }
    equals(MSVC_VER, 15.0) {
        MSVC_VER = "MSVC2017_64bit"
    }
CONFIG(release, debug|release): LIBS += -L$$PWD/'../BarcodeScanner-Desktop Qt $$[QT_VERSION] $$MSVC_VER/release/' -lBarcodeScanner
CONFIG(debug, debug|release): LIBS += -L$$PWD/'../BarcodeScanner-Desktop Qt $$[QT_VERSION] $$MSVC_VER/debug/' -lBarcodeScanner
CONFIG(release, debug|release): LIBS += -L$$PWD/'../CommonDevice-Desktop Qt $$[QT_VERSION] $$MSVC_VER/release/' -lCommonDevice
CONFIG(debug, debug|release): LIBS += -L$$PWD/'../CommonDevice-Desktop Qt $$[QT_VERSION] $$MSVC_VER/debug/' -lCommonDevice
CONFIG(release, debug|release): LIBS += -L$$PWD/'../MiPrinter-Desktop Qt $$[QT_VERSION] $$MSVC_VER/release/' -lMiPrinter
CONFIG(debug, debug|release): LIBS += -L$$PWD/'../MiPrinter-Desktop Qt $$[QT_VERSION] $$MSVC_VER/debug/' -lMiPrinter
CONFIG(release, debug|release): LIBS += -L$$PWD/'../DesktopPrinter-Desktop Qt $$[QT_VERSION] $$MSVC_VER/release/' -lDesktopPrinter
CONFIG(debug, debug|release): LIBS += -L$$PWD/'../DesktopPrinter-Desktop Qt $$[QT_VERSION] $$MSVC_VER/debug/' -lDesktopPrinter
CONFIG(release, debug|release): LIBS += -L$$PWD/../DesktopPrinter/limereport/$$MSVC_VER/ -llimereport
CONFIG(debug, debug|release): LIBS += -L$$PWD/../DesktopPrinter/limereport/$$MSVC_VER/ -llimereportd
}
