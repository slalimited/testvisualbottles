#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QMessageBox>
#include <QDateTime>
#include <cstdlib>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(&equip, SIGNAL(error(QString)), this, SLOT(error(QString)));
    connect(&equip, SIGNAL(warning(QString)), this, SLOT(warning(QString)));
    connect(&equip, SIGNAL(information(QString)), this, SLOT(information(QString)));
    connect(&equip, SIGNAL(newProductParameters(int, int, int, int)),
                           &equip, SLOT(setProductParameters(int, int, int, int)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    equip.productCalibration();
    equip.camerasBoxCodeAvailable(ui->lineEdit->text());
    /*QString codes = ui->lineEdit->text();
    QStringList list = codes.split(',', QString::SkipEmptyParts);
    qDebug() << list;
    int codeCount = list.count();
    QMultiMap<int, QString> xcoord, ycoord;
    for(int i = 0; i < codeCount; i++)
    {
        QStringList code = list[i].split(' ', QString::SkipEmptyParts);
        if(code.count() != 3)
        {
            qDebug() << "Error format";
            return;
        }
        xcoord.insert(code[1].toInt(), code[0]);
        ycoord.insert(code[2].toInt(), code[0]);
    }
    QVector<int> deltaX, deltaY;
    auto xIt = xcoord.begin();
    auto yIt = ycoord.begin();
    deltaXlimit = 0;
    deltaYlimit = 0;
    int dxMax = 0, dxMin = -1, dyMax = 0, dyMin = -1;
    for(int i = 0; i < codeCount-1; i++)
    {
        int x_prev = xIt.key();
        xIt++;
        int dx = xIt.key()-x_prev;
        deltaX.push_back(dx);
        int y_prev = yIt.key();
        yIt++;
        int dy = yIt.key()-y_prev;
        deltaY.push_back(dy);
        if(dx > dxMax) dxMax = dx;
        if(dx < dxMin || dxMin == -1) dxMin = dx;
        if(dy > dyMax) dyMax = dy;
        if(dy < dyMin || dyMin == -1) dyMin = dy;
    }
    deltaXlimit = (dxMax-dxMin)/2;
    deltaYlimit = (dyMax-dyMin)/2;
    qDebug() << deltaX << deltaXlimit;
    qDebug() << deltaY << deltaYlimit;
    xCount = 1;
    yCount = 1;
    for(int i = 0; i < deltaX.count(); i++)
        if(deltaX[i] < deltaXlimit)
        {
            qDebug() << deltaX[i];
            yCount++;
        }
        else
            break;
    for(int i = 0; i < deltaY.count(); i++)
        if(deltaY[i] < deltaYlimit)
        {
            qDebug() << deltaY[i];
            xCount++;
        }
        else
            break;
    if(codeCount == xCount*yCount)
        QMessageBox::information(this, "Обучение успешно",
                             tr("Отсканировано кодов: %5. Сетка: %1х%2. Порог ряда по Х: %3. "
                                "Порог ряда по Y: %4").arg(xCount).arg(yCount)
                             .arg(deltaXlimit).arg(deltaYlimit).arg(codeCount));
    else
        QMessageBox::critical(this, "Обучение неуспешно",
                             tr("Отсканировано кодов: %3. Сетка: %1х%2. Количество "
                                "отсканированных кодов не совпадает с предполагаемой сеткой!"
                                ).arg(xCount).arg(yCount)
                             .arg(codeCount)); */
}

void MainWindow::on_pushButton_2_clicked()
{
    equip.camerasBoxCodeAvailable(ui->lineEdit_2->text());
    /*quint64 t = QDateTime::currentMSecsSinceEpoch();
    ui->tableWidget->setRowCount(xCount);
    ui->tableWidget->setColumnCount(yCount); //000000001 381 401,
    QString codes = ui->lineEdit_2->text();
    QStringList list = codes.split(',', QString::SkipEmptyParts);
    qDebug() << list;
    int codesCount = list.count();
    if(codesCount == xCount*yCount)
    {
        //Все коды прочитаны
        for(int i = 0; i < xCount; i++)
            for(int j = 0; j < yCount; j++)
                ui->tableWidget->setItem(i, j, new QTableWidgetItem("OK"));
        ui->label->setText(tr("Затрачено времени: %1 мс").arg(
                               QDateTime::currentMSecsSinceEpoch()-t));
        return;
    }
    QMultiMap<int, QString> xcoord, yrow, ycoord;
    QMultiMap<QString, int> yValues;
    for(int i = 0; i < codesCount; i++)
    {
        QStringList code = list[i].split(' ', QString::SkipEmptyParts);
        if(code.count() != 3)
        {
            QMessageBox::critical(this, tr("Ошибка"),
                                  tr("Неверный формат входных данных"));
            ui->label->setText(tr("Затрачено времени: %1 мс").arg(
                                   QDateTime::currentMSecsSinceEpoch()-t));
            return;
        }
        xcoord.insert(code[1].toInt(), code[0]);
        ycoord.insert(code[2].toInt(), code[0]);
        yValues.insert(code[0], code[2].toInt());
    }
    int xDiff = 0, yDiff = 0;
    auto xIt = xcoord.begin();
    auto yIt = ycoord.begin();
    int lastX = xIt.key();
    int lastY = yIt.key();
    QVector<int> xPos, yPos;
    int xSum = lastX;
    int ySum = lastY;
    int xC = 1;
    int yC = 1;
    for(int i = 0; i < codesCount; i++)
    {
        xIt++;
        yIt++;
        if(xIt.key()-lastX > deltaXlimit)
        {
            xPos.push_back(xSum/xC);
            xDiff++;
        }
        else
        {
            xSum+= xIt.key();
            xC++;
        }
        if(yIt.key()-lastY > deltaYlimit)
        {
            yPos.push_back(ySum/yC);
            yDiff++;
        }
        else
        {
            ySum+= yIt.key();
            yC++;
        }
        lastX = xIt.key();
        xSum = lastX;
        xC = 1;
        lastY = yIt.key();
        ySum = lastY;
        yC = 1;
    }
    qDebug() << "xDiff" << xDiff << xPos;
    qDebug() << "yDiff" << yDiff << yPos;
    if(xDiff < xCount || yDiff < yCount)
    {
        QMessageBox::critical(this, tr("Ошибка"),
                              tr("Невозможно определить позиции кодов"));
        ui->label->setText(tr("Затрачено времени: %1 мс").arg(
                               QDateTime::currentMSecsSinceEpoch()-t));
        return;
    }
    xIt = xcoord.begin();
    lastX = xIt.key();
    int curRow = 0;
    yrow.insert(yValues.value(xIt.value()), xIt.value());
    qDebug() << xcoord;
    for(int i = 0; i < codesCount; i++)
    {
        xIt++;
        if((xIt.key()-lastX) < deltaXlimit)
            yrow.insert(yValues.value(xIt.value()), xIt.value());
        else
        { //Следующий ряд, визуализируем
            qDebug() << yrow;
            auto code = yrow.begin();
            for(int j = 0; j < yCount; j++)
            {
                QString text = "NoRead";
                if(code != yrow.end())
                    if(abs(ycoord.key(code.value())-yPos[j]) < deltaYlimit)
                    {
                        text = code.value();
                        code++;
                    }
                //qDebug() << xcoord.key(code.value()) << xPos[j];
                ui->tableWidget->setItem(curRow, j, new QTableWidgetItem(text));
                qDebug() << "insert" << curRow << j << text;
            }
            curRow++;
            yrow.clear();
            if(xIt != xcoord.end())
                yrow.insert(yValues.value(xIt.value()), xIt.value());
        }
        lastX = xIt.key();
    }
    ui->label->setText(tr("Затрачено времени: %1 мс").arg(
                           QDateTime::currentMSecsSinceEpoch()-t));
                           */
}

void MainWindow::error(QString str)
{
    ui->log->append("Ошибка:" + str);
}

void MainWindow::warning(QString str)
{
    ui->log->append("Варнинг:" + str);
}

void MainWindow::information(QString str)
{
    ui->log->append("Инфо:" + str);
}
